from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    """Хранит инфу о профиле"""
    STATUS_CHOICES = (
        ('Basic', 'Basic'),
        ('VIP', 'VIP'),
        ('Premium', 'Premium')
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.TextField()
    place = models.CharField(max_length=300)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='Basic')
    time_last_edit = models.DateTimeField(auto_now=True)
    time_for_listing = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.user}'

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'


class LikedProfile(models.Model):
    """Хранит понравившиеся профили конкретного юзера"""
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    favorite_profile = models.ManyToManyField(Profile, related_name='favorite_profile')

    def __str__(self):
        return f'{self.user}. Понравившиеся'

    class Meta:
        verbose_name = 'Понравившиеся'
        verbose_name_plural = 'Понравившиеся'


class Message(models.Model):
    """Хранит все сообщения и инфу о них"""
    sender = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='sender')
    addressee = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='addressee')
    text = models.TextField()

    def __str__(self):
        return f'Cообщение №{self.id}. Отправитель: {self.sender}. Получатель: {self.addressee}'

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'


class WatchedProfiles(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    watched_profile = models.ForeignKey(Profile, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user} просмотрел профиль {self.watched_profile.user}'

    class Meta:
        verbose_name = 'Просмотренное'
        verbose_name_plural = 'Просмотренное'


class UserImage(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to='images/')

    def __str__(self):
        return f'Фотография юзера {self.profile.user}'

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'
