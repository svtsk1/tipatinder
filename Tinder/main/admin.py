from django.contrib import admin
from Tinder.main.models import Profile, LikedProfile, Message, WatchedProfiles, UserImage

admin.site.register(Profile)
admin.site.register(LikedProfile)
admin.site.register(Message)
admin.site.register(WatchedProfiles)
admin.site.register(UserImage)
