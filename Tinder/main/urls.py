from django.urls import path, include
from Tinder.main.views import ProfileViewSet, LikedProfileViewSet, SendMessageViewSet, ChangePlaceViewSet, \
    AddLikeViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('all', ProfileViewSet, basename='ProfileView')
router.register('your_likes', LikedProfileViewSet, basename='LikesView')
router.register('message', SendMessageViewSet, basename='SendMessageView')
router.register('update_place', ChangePlaceViewSet, basename='ChangePlaceView')
router.register('add_like', AddLikeViewSet, basename='AddLikeView')

urlpatterns = [
    path('', include(router.urls))
]
