import datetime
from django.shortcuts import get_object_or_404
from geopy.geocoders import Nominatim
from geopy.distance import geodesic
from pytz import utc
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from Tinder.main.models import Profile, LikedProfile, Message, WatchedProfiles
from rest_framework import viewsets, mixins, status, permissions
from rest_framework.views import APIView
from Tinder.main.serializers import ProfileSerializer, LikedProfileSerializer, MessageSerializer, ChangePlaceSerializer, \
    AddLikeSerializer


class ProfileViewSet(viewsets.ModelViewSet):
    """Все анкеты, которые доступны в зависимости от подписки"""
    serializer_class = ProfileSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        """Отбирает профили в зависимости от расстояния. Выдаёт определённое кол-во
        в зависимости от подписки.
        Смотрит, когда последний раз анкеты обновлялись, если прошло больше суток,
        то заново фильтрует все анкеты в зависимости от условий подписки и сохраняет их как просмотренные.
        Если прошло меньше суток, то просто берёт все просмотренные посты конкретного юзера и опять их показвает."""
        queryset = []
        geolocator = Nominatim(user_agent='Tinder.main')
        all_profiles = Profile.objects.all()
        user_profile = get_object_or_404(Profile, user=self.request.user)
        status = user_profile.status
        if status == 'Basic':
            status_distance = 10
            profile_count = 20
        if status == 'VIP':
            status_distance = 25
            profile_count = 100
        if status == 'Premium':
            status_distance = 5555
            profile_count = 10000

        delta = datetime.datetime.now(utc) - user_profile.time_for_listing
        hours = delta.total_seconds() / 3600

        if hours > 24:
            user_profile.time_for_listing = datetime.datetime.now(utc)
            user_profile.save()
            place = geolocator.geocode(user_profile.place)
            pointA = (place.latitude, place.longitude)
            profiles_for_user = all_profiles[:profile_count]

            watched_profiles = WatchedProfiles.objects.filter(user=self.request.user)
            for profile in watched_profiles:
                profile.delete()

            for profile in profiles_for_user:
                place = geolocator.geocode(profile.place)
                pointB = (place.latitude, place.longitude)
                distance = round(geodesic(pointA, pointB).km, 2)
                if distance < status_distance:
                    queryset.append(profile)
                    w = WatchedProfiles.objects.create(user=self.request.user, watched_profile=profile)
            return queryset

        else:
            queryset = []
            profiles = WatchedProfiles.objects.filter(user=self.request.user)
            for profile in profiles:
                queryset.append(profile.watched_profile)
            return queryset


class ChangePlaceViewSet(mixins.UpdateModelMixin, viewsets.GenericViewSet):
    """Если с последнего обновления точки прошло больше двух часов,
    то сохраняет в базе данных, иначе не сохраняет"""
    serializer_class = ChangePlaceSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = Profile.objects.all()

    def update(self, request, *args, **kwargs):
        profile = Profile.objects.get(user=self.request.user)
        delta = datetime.datetime.now(utc) - profile.time_last_edit
        hours = delta.total_seconds()/3600
        print(profile.time_last_edit)

        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        if hours > 2:
            profile.time_last_edit = datetime.datetime.now(utc)
            profile.save()
            self.perform_update(serializer)

            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}

            return Response(serializer.data)

        else:
            return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)


class LikedProfileViewSet(viewsets.ReadOnlyModelViewSet):
    """Анкеты, которые лайкнул юзер"""
    serializer_class = LikedProfileSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        user = self.request.user
        return LikedProfile.objects.filter(user=user)

    def update(self, request, pk=None):
        pass


class SendMessageViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, GenericViewSet):
    """Просмотр своих сообщений. Отправка сообщения юзера, с которым мэтч"""
    serializer_class = MessageSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        user = self.request.user
        user_profile = get_object_or_404(Profile, user=user)
        return Message.objects.filter(sender=user_profile)

    def create(self, request):
        """Если введённая инфа валидная и отправитель есть в списке профилей,
        которые понравились получателю, то всё сохраняется в БД и возвращается респонс,
        если мэтча нет, то ничего не сохраняет, а просто выводит обратно респонс"""
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        sender_profile = get_object_or_404(Profile, id=request.data['sender'])
        addressee_profile = get_object_or_404(Profile, id=request.data['addressee'])
        addressee_likes = get_object_or_404(LikedProfile, user=addressee_profile.user)

        if sender_profile in addressee_likes.favorite_profile.all():
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

        else:
            return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)


class AddLikeViewSet(mixins.UpdateModelMixin, viewsets.GenericViewSet):
    serializer_class = AddLikeSerializer
    queryset = LikedProfile.objects.all()
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
