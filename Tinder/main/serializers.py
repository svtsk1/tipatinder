from Tinder.main.models import Profile, LikedProfile, Message, UserImage
from rest_framework import serializers


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserImage
        fields = (
            'image',
        )


class ProfileSerializer(serializers.ModelSerializer):
    images = ImageSerializer(many=True)

    class Meta:
        model = Profile
        fields = (
            'id',
            'user',
            'description',
            'place',
            'status',
            'time_last_edit',
            'time_for_listing',
            'images',
        )


class LikedProfileSerializer(serializers.ModelSerializer):
    favorite_profile = ProfileSerializer(many=True)

    class Meta:
        model = LikedProfile
        fields = (
            'user',
            'favorite_profile'
        )


class MessageSerializer(serializers.ModelSerializer):
    sender = ProfileSerializer
    addressee = ProfileSerializer

    class Meta:
        model = Message
        fields = (
            'sender',
            'addressee',
            'text'
        )


class ChangePlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = (
            'place',
        )


class AddLikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = LikedProfile
        fields = (
            'user',
            'favorite_profile',
        )
