from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('profiles/', include('Tinder.main.urls')),
    path('auth/', include('djoser.urls')),
    # path('auth/token', obtain_auth_token, name='token'),
    path('token/', include('djoser.urls')),
    path('token/refresh', include('djoser.urls')),
]
